# bank message log

Projeto leitura mensageria, ler protocolo amqp (broker rabbitmq) e guardar mensagem no banco de dados. Possui API rest para manipular mensagems

## install

```sh
poetry install
```

Copiar arquivo _config.sample.yml_ e renomear para _config.yml_

Obs: garantir que o banco postgres e rabbitmq (ou outro broker) esta sendo executando e fila descrita no config esteja cadastrada.

## Run migration (database)

```sh
alembic upgrade head
```

util commands:

```sh
 alembic history
 alembic downgrade -1
```

## run

```sh
uvicorn src.app:app --port 5000
```

ou

```sh
make start
```

## run dev

```sh
make dev
```

obs: executar vscode laucher para debbugar

## Api docs

<http://localhost:5000/redoc>

## Test

Testes unitários

```sh
python -m pytest -v
make test_unit
```

### Coverage

```sh
make test_cov
python -m pytest --cov=src
```

---
