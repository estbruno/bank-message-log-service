from types import FunctionType
import uuid
import threading


def generate_uuid():
    return str(uuid.uuid4())


class ThreadBase(threading.Thread):
    def __init__(self, execution: FunctionType):
        threading.Thread.__init__(self, target=execution)
