from src.commons.helpers import ThreadBase
from src.commons.amqp_service import AmqpService
from src.modules.message.services import MessageService
from src.modules.message.models import MessageCreateDto


class MessageConsumer:

    def __init__(self, amqp_service: AmqpService, message_service: MessageService, message_queue: str) -> None:
        self._message_service: MessageService = message_service
        self._channel = amqp_service.create_channel()
        self._channel.basic_consume(message_queue, self.on_message)

    def start_thread(self):
        self._thread = ThreadBase(self._channel.start_consuming)
        self._thread.daemon = True
        self._thread.start()

    def on_message(self, channel, method_frame, header_frame, body) -> None:
        print(body)
        try:
            dto: MessageCreateDto
            if isinstance(body, dict):
                dto: MessageCreateDto = MessageCreateDto(content=body)
            else:
                dto: MessageCreateDto = MessageCreateDto(
                    content={'message': str(body)})
            self.create_message(channel, method_frame, dto)
        except KeyboardInterrupt:
            self._channel.stop_consuming()
        except IsADirectoryError:
            print('IsADirectoryError')

    def create_message(self, channel, method_frame, dto) -> None:
        result = self._message_service.create(dto)
        print(result)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)

    def stop_consuming(self) -> None:
        self._channel.stop_consuming()
