from pika import BlockingConnection, URLParameters
from pika.adapters.blocking_connection import BlockingChannel


class AmqpService:

    def __init__(self, broker_url: str) -> None:
        self._connection: BlockingConnection = BlockingConnection(
            URLParameters(broker_url)
        )

    def get_connection(self) -> BlockingConnection:
        return self._connection

    def close_connection(self) -> None:
        self._connection.close()

    def create_channel(self) -> BlockingChannel:
        return self._connection.channel()
