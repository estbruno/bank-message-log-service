"""Application module."""

import sys
from typing import List
from fastapi import FastAPI

from src.containers import ApplicationContainer
from src.modules.message import controller as message_controller


class FastApiContainer(FastAPI):
    container: ApplicationContainer


def get_modules() -> List[any]:
    return [message_controller]


def set_routes(app: FastAPI) -> None:
    app.include_router(message_controller.router)


def set_consumers(container: ApplicationContainer) -> None:
   container.message_consumer().start_thread()

def create_app() -> FastApiContainer:
    container: ApplicationContainer = ApplicationContainer()
    container.config.from_yaml('config.yml')
    container.wire(modules=get_modules())

    app = FastAPI()
    app.container = container
    set_routes(app)
    set_consumers(container)

    return app


app = create_app()


@app.on_event("shutdown")
def shutdown_event():
    sys.exit()
