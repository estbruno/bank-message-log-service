from dependency_injector import containers, providers

from src.commons.database import Database
from src.modules.message.container import MessageContainer
from src.commons.amqp_service import AmqpService
from src.commons.amqp_service.message_consumer import MessageConsumer


class ApplicationContainer(containers.DeclarativeContainer):

    config = providers.Configuration()

    db: Database = providers.Singleton(Database, db_url=config.database.url)

    amqp_service: AmqpService = providers.Singleton(
        AmqpService, broker_url=config.amqp_broker.url)

    message: MessageContainer = providers.Container(
        MessageContainer,
        config=config,
    )

    message_consumer: MessageConsumer = providers.Singleton(
        MessageConsumer,
        amqp_service=amqp_service,
        message_service=message.service,
        message_queue=config.amqp_broker.message_queue
    )
