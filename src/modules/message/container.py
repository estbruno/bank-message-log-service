from dependency_injector import containers, providers

from src.commons.database import Database
from .services import MessageService
from .repositories import MessageRepository


class MessageContainer(containers.DeclarativeContainer):

    config = providers.Configuration()

    db: Database = providers.Singleton(Database, db_url=config.database.url)

    repository: MessageRepository = providers.Factory(
        MessageRepository,
        session_factory=db.provided.session,
    )

    service: MessageService = providers.Factory(
        MessageService,
        repository=repository,
    )
