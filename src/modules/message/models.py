
from datetime import date, datetime
from sqlalchemy import Column, String, Boolean, Integer, DateTime
from typing import NamedTuple, Dict
from pydantic import BaseModel
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy_json import mutable_json_type

from src.commons.database import Base
from src.commons.helpers import generate_uuid


class Message(Base):

    __tablename__ = 'messages'

    id = Column(UUID(as_uuid=True), primary_key=True, default=generate_uuid)
    content = Column(mutable_json_type(
        dbtype=JSONB, nested=True), nullable=False)
    hide = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.now())

    def as_dict(self):
        return {c.content: getattr(self, c.content) for c in self.__table__.columns}

    def __repr__(self):
        return f'<Message(id="{self.id}", ' \
            f'content="{self.content}", ' \
            f'hide="{self.hide}")>'


class MessageListDto(NamedTuple):
    id: str
    content: int
    hide: bool
    created_at: date


class MessageCreateDto(BaseModel):
    content: Dict


class MessageUpdateDto(BaseModel):
    content: Dict
    hide: bool

    def asdict(self):
        return {'content': self.content, 'hide': self.hide}
