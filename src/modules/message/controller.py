"""Views module."""

from src.modules.message.repositories import NotFoundError
from fastapi import APIRouter, Depends, Response, status, Request
from dependency_injector.wiring import inject, Provide

from src.containers import ApplicationContainer

from .services import MessageService
from .models import MessageCreateDto, MessageUpdateDto


router = APIRouter()


@router.get('/message')
@inject
def get_list(
    message_service: MessageService = Depends(
        Provide[ApplicationContainer.message.service])
):
    return message_service.find()


@router.post('/message', status_code=status.HTTP_201_CREATED)
@inject
def create(
    payload: MessageCreateDto,
    message_service: MessageService = Depends(
        Provide[ApplicationContainer.message.service])
):
    return message_service.create(payload)


@router.get('/message/{id}')
@inject
def get_by_id(
    id: str,
    message_service: MessageService = Depends(Provide[ApplicationContainer.message.service]),
):
    try:
        return message_service.get_by_id(id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)


@router.delete('/message/{id}', status_code=status.HTTP_204_NO_CONTENT)
@inject
def remove(
    id: str,
    message_service: MessageService = Depends(Provide[ApplicationContainer.message.service]),
):
    try:
        message_service.delete_by_id(id)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put('/message/{id}', status_code=status.HTTP_200_OK)
@inject
def update(
    id: str,
    payload: MessageUpdateDto,
    message_service: MessageService = Depends(Provide[ApplicationContainer.message.service]),
):
    try:
        return message_service.update_by_id(id, payload)
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
