"""Services module."""

from typing import Iterator
from sqlalchemy.sql.sqltypes import String

from .repositories import MessageRepository
from .models import Message, MessageListDto, MessageCreateDto, MessageUpdateDto


class MessageService:

    def __init__(self, repository: MessageRepository) -> None:
        self._repository: MessageRepository = repository

    def find(self) -> Iterator[MessageListDto]:
        return self._repository.get_all()

    def get_by_id(self, id: str) -> Message:
        return self._repository.get_by_id(id)

    def create(self, dto: MessageCreateDto) -> Message:
        return self._repository.add(content=dto.content)

    def delete_by_id(self, id: str) -> None:
        return self._repository.delete_by_id(id)

    def update_by_id(self, id: str, dto=MessageUpdateDto) -> None:
        return self._repository.update(id, dto)
