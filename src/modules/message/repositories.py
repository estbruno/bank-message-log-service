"""Repositories module."""

from typing import Iterator

from sqlalchemy.orm import Session
from sqlalchemy.sql.sqltypes import String

from .models import Message, MessageUpdateDto


class MessageRepository:

    def __init__(self, session_factory) -> None:
        self.session_factory: Session = session_factory

    def get_all(self) -> Iterator[Message]:
        with self.session_factory() as session:
            return session.query(Message).all()

    def get_by_id(self, id: str) -> Message:
        with self.session_factory() as session:
            data = session.query(Message).filter(Message.id == id).first()
            if not data:
                raise MessageNotFoundError(id)
            return data

    def add(self, content: str) -> Message:
        with self.session_factory() as session:
            data = Message(content=content)
            session.add(data)
            session.commit()
            session.refresh(data)
            return data

    def delete_by_id(self, id: str) -> None:
        with self.session_factory() as session:
            entity: Message = session.query(
                Message).filter(Message.id == id).first()
            if not entity:
                raise MessageNotFoundError(id)
            session.delete(entity)
            session.commit()

    def update(self, id: str, message: MessageUpdateDto) -> Message:
        with self.session_factory() as session:
            entity: Message = session.query(Message).filter(
                Message.id == id
            )
            entity.update(message.asdict())
            if not entity:
                session.rollback()
                raise MessageNotFoundError(id)
            session.commit()
            return entity.first()


class NotFoundError(Exception):

    entity_name: str

    def __init__(self, entity_id):
        super().__init__(f'{self.entity_name} not found, id: {entity_id}')


class MessageNotFoundError(NotFoundError):

    entity_name: str = 'Message'
