"""Tests module."""

from datetime import datetime
from unittest import mock

import pytest
from fastapi.testclient import TestClient

from src.commons.helpers import generate_uuid
from src.modules.message.repositories import MessageRepository, MessageNotFoundError
from src.modules.message.models import Message
from src.app import app


@pytest.fixture
def client():
    yield TestClient(app)


def test_get_list(client):
    message_one = Message(id=generate_uuid(), content={"teste": "teste"},
                          hide=True, created_at=datetime.now())
    message_two = Message(id=generate_uuid(), content={"teste2": ["teste1", "teste2"]},
                          hide=False, created_at=datetime.now())
    repository_mock = mock.Mock(spec=MessageRepository)
    repository_mock.get_all.return_value = [message_one, message_two]

    with app.container.message.repository.override(repository_mock):
        response = client.get('/message')

    assert response.status_code == 200
    data = response.json()
    print(data)
    print('--------------------')
    data_to_check = [
        {'id': message_one.id, 'content': message_one.content,
            'hide': message_one.hide, 'created_at': "{:%Y-%m-%dT%H:%M:%S%z.%f}".format(message_one.created_at)},
        {'id': message_two.id, 'content': message_two.content,
            'hide': message_two.hide, 'created_at': "{:%Y-%m-%dT%H:%M:%S%z.%f}".format(message_two.created_at)},
    ]
    print(data_to_check)
    assert data == data_to_check


def test_get_by_id(client):
    message_one = Message(id=generate_uuid(), content={"teste": "teste"},
                          hide=True, created_at=datetime.now())
    repository_mock = mock.Mock(spec=MessageRepository)
    repository_mock.get_by_id.return_value = message_one

    with app.container.message.repository.override(repository_mock):
        response = client.get('/message/'+message_one.id)

    assert response.status_code == 200
    data = response.json()
    assert data == {'id': message_one.id, 'content': message_one.content,
                    'hide': message_one.hide, 'created_at': "{:%Y-%m-%dT%H:%M:%S%z.%f}".format(message_one.created_at)}
    repository_mock.get_by_id.assert_called_once_with(message_one.id)


def test_get_by_id_404(client):
    repository_mock = mock.Mock(spec=MessageRepository)
    repository_mock.get_by_id.side_effect = MessageNotFoundError(1)

    with app.container.message.repository.override(repository_mock):
        response = client.get('/message/1')

    assert response.status_code == 404


# @mock.patch('webapp.services.uuid4', return_value='12345')
def test_add(client):
    content = {"teste": "teste"}
    message_one = Message(id=generate_uuid(), content=content,
                          hide=True, created_at=datetime.now())
    repository_mock = mock.Mock(spec=MessageRepository)
    repository_mock.add.return_value = message_one

    with app.container.message.repository.override(repository_mock):
        response = client.post(
            '/message', json={"content": content})

    assert response.status_code == 201
    data = response.json()
    assert data == {'id': message_one.id, 'content': message_one.content,
                    'hide': message_one.hide, 'created_at': "{:%Y-%m-%dT%H:%M:%S%z.%f}".format(message_one.created_at)}
    repository_mock.add.assert_called_once_with(content=content)


def test_remove(client):
    repository_mock = mock.Mock(spec=MessageRepository)
    id_uuid = generate_uuid()
    with app.container.message.repository.override(repository_mock):
        response = client.delete('/message/' + id_uuid)

    assert response.status_code == 204
    repository_mock.delete_by_id.assert_called_once_with(id_uuid)


def test_remove_404(client):
    repository_mock = mock.Mock(spec=MessageRepository)
    repository_mock.delete_by_id.side_effect = MessageNotFoundError(1)

    id_uuid = generate_uuid()
    with app.container.message.repository.override(repository_mock):
        response = client.delete('/message/' + id_uuid)

    assert response.status_code == 404
