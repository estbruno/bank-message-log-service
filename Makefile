
default: start

install:
	poetry install

start:
	python -m uvicorn src.app:app --port 5000

dev:
	python -m uvicorn src.app:app --port 5000 --reload

test_unit:
	python -m pytest -v

test_cov:
	python -m pytest --cov=src