"""create message table

Revision ID: 8f6d2dd778d6
Revises:
Create Date: 2021-07-02 18:47:47.079588

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB, UUID
import uuid
from sqlalchemy_json import mutable_json_type
from datetime import  datetime

# revision identifiers, used by Alembic.
revision = '8f6d2dd778d6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'messages',
        sa.Column('id', UUID(as_uuid=True), primary_key=True, default=str(uuid.uuid4())),
        sa.Column('content', mutable_json_type(dbtype=JSONB, nested=True), nullable=False),
        sa.Column('hide', sa.Boolean, default=False),
        sa.Column('created_at', sa.DateTime, default=datetime.now())
    )


def downgrade():
    op.drop_table('messages')
